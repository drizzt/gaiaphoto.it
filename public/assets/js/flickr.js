$(function () {
    'use strict';

    $.ajax({
        url: 'https://api.flickr.com/services/rest/',
        data: {
            photoset_id: '72157702206821681',
            user_id: '96610528@N06',
            format: 'json',
            method: 'flickr.photosets.getPhotos',
            extras: 'url_m,url_o',
            api_key: 'f6fdb5a636863d148afa8e7bb056bf1b', // jshint ignore:line
        },
        dataType: 'jsonp',
        jsonp: 'jsoncallback'
    }).done(function (result) {

        var linksContainer = $('#two .row'),
            baseUrl, myImg, aClass;

        $.each(result.photoset.photo, function (index, photo) {
            myImg = $('<img>').prop('src', photo.url_m);
            if (myImg[0].height > myImg[0].width)
                myImg.addClass('portrait');

            $('<article/>')
                .addClass('col-6 col-12-xsmall work-item')
                .append($('<a/>')
                    .addClass('image fit thumb')
                    .append(myImg)
                    .prop('href', photo.url_o)
                    .prop('title', photo.title))
                .append($('<h3>' + photo.title + '</h3>'))
                .appendTo(linksContainer);
        });
    });

});
